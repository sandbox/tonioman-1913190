(function ($) {
Drupal.behaviors.menuBlockMobileExpand = {
    attach: function (context, settings) {
      $('body', context).once('menuExpand', function () {
    	var mobileMenuIds = Drupal.settings.menu_block_mobile.mobile_menu_ids;

    	if (mobileMenuIds !== '') {	

    		// Split the list into an array of values
    		var menuids = mobileMenuIds.split(', ');
    		
    		// Loop through each value to make them each expandable menus
            $.each( menuids, function( key, menuid ) {
            	//Define your drupal menu id (only works with core menu block)
     	            
            	// Change menuid into a CSS ID format.
            	menuid = '#' + menuid;
	
            	// Add a class to menu div that has menu id so that generalized CSS will work.
            	$(menuid).addClass("menu-expandable-block");
            	$(menuid+" h2.block-title").before( $('<span class="menu-block-over-top" href="#">Expand ↓</span>') );
     	   //     $(menuid +" ul.menu:not(ul.menu li ul.menu)").before( $('<span class="menu-block-over" href="#">Expand ↓</span>') );    
 	            $(menuid +' ul.menu:not(ul.menu li ul.menu)').slideUp('fast'); 

     	         //Add a span tag that will act as the expand button, you can change the output of that button here
     	        $(menuid +" ul.menu li.expanded").prepend( $('<span class="menu-block-over" href="#">Expand ↓</span>') );    
 	            $(menuid +' span.menu-block-over').siblings('ul').slideUp('fast'); 

     	        //Create an open/close action on the accordion after clicking on the expand element        
         	    $(menuid +' span.menu-block-over').click(function (event) {    
     	          event.preventDefault();
     	          if ($(this).siblings('ul').is( ":visible" )) {
     	            $(this).siblings('ul').slideUp('fast'); 
     		        $(this).html('Expand ↓');
     	          } 
     	          else {          
     	            $(this).siblings('ul').slideDown('fast'); 
     		        $(this).html('Collapse ↑');
     	          }                      
     	        });  
         	    
         	    
         	    
         	 //Create an open/close action on the accordion after clicking on the expand element        
         	    $(menuid +' .menu-block-over-top').click(function (event) {    
     	          event.preventDefault();
     	          if ($(menuid+' div ul.menu:not(ul.menu li ul.menu)').is( ":visible" )) {
     	            $(menuid+' div ul.menu:not(ul.menu li ul.menu)').slideUp('fast'); 
     		        $(this).html('Expand ↓');
     	          }
     	          else {          
     	            $(menuid +' div ul.menu:not(ul.menu li ul.menu)').slideDown('fast'); 
     		        $(this).html('Collapse ↑');
     	          }
     	        });
         	    
         	    
         	    
            });
    	}
      });
    }
  }   
}(jQuery));